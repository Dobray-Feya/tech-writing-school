.. _contribute_to_open_source:

Contributing to Open Source
===========================

There are multiple contribution and development models that a project could follow.
Here we will show the flow for this project.

.. note::
   We only discuss the basics of the contribution process in a web browser.
   Cloning the repository to work on it locally is not covered here.
   To work on a real project, you would need to :ref:`learn to use git <learn_git>` either through a command line
   or a desktop application.

.. _contribute_fork:

Fork a Repository
*****************

Fork is your own copy of a repository. Some projects do not allow you to create branches, so you have to create a fork
to contribute to that project.

To fork a repository, click ``Fork`` in the right upper corner:

.. image:: images/fork-a-repo.JPG
   :width: 800

After you fork the repository, you will be redirected to the fork:

.. image:: images/after-fork.JPG
   :width: 800

After a repository is forked, you may :ref:`create a new branch <contribute_branch>`,
introduce changes, and then :ref:`create a merge request <contribute_merge_request>` to
merge your changes into the original repository.

.. seealso::

   - `Fork a repository (GitLab) <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`_
   - `Fork a repository (GitHub) <https://docs.github.com/en/github/getting-started-with-github/fork-a-repo>`_

.. _contribute_branch:

Create a Branch
***************

We create branches to work independently from other contributors.

To create a new branch, go to your fork and click on the "plus" sign to the right of the repository name.
Note that you can create branches from different branches, not only ``main``.

.. image:: images/add-new-branch-after-fork.JPG
   :width: 800

Introduce changes to source files in the branch you have created. Once you are done,
:ref:`create a merge request <contribute_merge_request>` to merge your changes.

.. seealso::

   - :ref:`learn_git`

.. _contribute_merge_request:

Create a Merge (Pull) Request
******************************

Merge request in Gitlab and pull requests on Github are essentially the same thing.
When you are ready to show your work to others and to merge your changes to the main development branch,
you create a merge (pull) request and ask the maintainers of the repository to review and approve your changes.

After you make any changes in your development branch, you will be promted to create a new merge request:

.. image:: images/after-push.JPG
   :width: 800

After you click ``Create merge request``, you will be asked to fill in various fields such as title and description:

.. image:: images/new-merge-request.JPG
   :width: 800

.. note::
   For this sandbox exercise, you do not need to worry about other fields.
   Add a brief description and a title, and click ``Submit merge request``.

.. seealso::

   - `Getting started with Merge Requests (GitLab docs) <https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html>`_
   - `How to create a merge request (GitLab docs) <https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html>`_

Tests
-----

When you create a merge request or add new commits to it, the CI pipeline with tests starts running.
If all the tests were passed, the CI is green.

.. tabs::

   .. tab:: The pipeline running

      .. image:: images/pipeline-running.JPG
         :width: 800

   .. tab:: The pipeline succeeded

      .. image:: images/pipeline-succeeded.JPG
         :width: 800

Publising
---------

After your merge request was approved and merged by the repository owner, the publishing pipeline
starts running. Once it is passed, you can go to the website and check the updates.

.. tabs::

   .. tab:: The publishing pipeline running

      .. image:: images/publishing-pipeline-running.JPG
         :width: 800

   .. tab:: The publishing pipeline succeeded

      .. image:: images/publishing-pipeline-succeeded.JPG
         :width: 800

.. seealso::

   - :ref:`ci_cd`